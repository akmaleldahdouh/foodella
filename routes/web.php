<?php

use Illuminate\Support\Facades\Route;



Route::get('/', function () {
    return view('layout.base');
});

Route::get('admin/login', 'Admin\Auth\AuthController@showLoginForm');
Route::post('admin/login', 'Admin\Auth\AuthController@login')->name('admin_login');
Route::post('admin/logout', 'Admin\Auth\AuthController@logout')->name('admin_logout');

Route::get('admin/dashboard', 'Admin\TestController@index')->name('admin.dashboard')->middleware('auth:admin');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['auth:admin']], function () {

    Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
        Route::get('/{id}/edit', 'ProfileController@edit')->name('edit');
        Route::put('/{id}', 'ProfileController@update')->name('update');
    });

});
