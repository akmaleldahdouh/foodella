<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function edit($id)
    {
        $admin = Admin::query()->findOrFail($id);
        return view('admin.auth.profile', compact('admin'));
    }


}
