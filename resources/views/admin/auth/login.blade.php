
<!doctype html>
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8">
    <title>Gift Cards Distribution</title>
    <meta name="description" content="Updates and statistics">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Fonts -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
    <link rel="stylesheet" href="{{ asset('template/assets/css/pages/login/style.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('template/assets/css/pages/login/vendors.bundle.css') }}">
    <style>
        @font-face {
            font-family: Miriam Libre;
        url('{{ public_path('fonts/line-awesome/line-awesome.woff2') }}');
        }

        .bg-black-op{
            height:100%;background-color:rgba(0, 0, 0, 0.4);
        }

        .hero-static {
            min-height: 100vh;
        }

        .font-w600 {
            font-weight: 600!important;
        }

        .font-size-h3 {
            font-size: 1.857143rem;
        }

        .text-white-op {
            color: rgba(255,255,255,.8)!important;
        }

        .font-italic {
            font-style: italic!important;
        }

        .content {
            margin: 0 auto;
            padding: 12px 12px 1px;
            width: 100%;
            overflow-x: visible;
        }

        #main-container{
            overflow-x: hidden;
        }
    </style>
    <title>GiftLov - Admin Area</title>
</head>
<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<div id="page-container" class="side-trans-enabled">
    <main id="main-container">
        <div class="bg-image" style=" background-size: cover; background-image: url('{{ asset('template/assets/media/login/login.jpg') }}');">
            <div class="row kt-margin-x bg-black-op">
                <div class="hero-static col-md-6 col-xl-8 d-none d-md-flex align-items-md-end animated fadeInRight">
                    <div class="kt-padding-30 js-appear-enabled animated fadeIn" data-toggle="appear">
                        <p class="font-size-h3 font-w600 text-white"> Get Inspired and Create! </p>
                        <p class="font-italic text-white-op"> Copyright � <span class="js-year-copy js-year-copy-enabled"><script>document.write(new Date().getFullYear())</script></span> </p>
                    </div>
                </div>
                <div class="hero-static kt-padding-r-25 kt-padding-l-15 col-md-6 col-xl-4 d-flex align-items-center bg-white js-appear-enabled animated fadeInRight" data-toggle="appear" data-class="animated fadeInRight">
                    <div class="content">
                        <div class="row">
                            <div class="col kt-padding-r-30 kt-padding-l-30 kt-padding-t-10 kt-padding-b-10">
                                <h1 class="h2 font-weight-500 text-dark kt-margin-t-30 kt-margin-b-10">Welcome to <span class="text-danger">GiftLov <i class="la la-heart"></i>!</span></h1>
                                <h2 class="h5 font-weight-light text-dark kt-margin-b-0">Please sign in</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col kt-padding-r-30 kt-padding-l-30 kt-padding-t-10 kt-padding-b-10">
                                <!--begin::Form-->
                                <form class="kt-form" method="POST" action="{{ route('admin_login') }}">
                                    {{ csrf_field() }}
                                    <div class="kt-portlet__body">
                                        <div class="kt-section kt-section--first">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email">
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" name="password" id="password" class="form-control" placeholder="Enter password">
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label class="kt-checkbox"> <input type="checkbox" name="remember"> Remember me <span></span> </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <button type="submit" class="btn btn-primary"><i class="la la-sign-in"></i>&nbsp;Login</button>
                                        </div>
                                    </div>
                                </form>
                                <!--end::Form-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>


</body>
</html>
